<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\FixedPercent */

$this->title = 'Create Fixed Percent';
$this->params['breadcrumbs'][] = ['label' => 'Fixed Percent', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fixed-percent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
