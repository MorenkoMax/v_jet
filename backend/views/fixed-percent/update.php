<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FixedPercent */

$this->title = 'Update Fixed Percent: ' . ' ' . $model->percent;
$this->params['breadcrumbs'][] = ['label' => 'Fixed Percent', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->percent, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fixed-percent-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
