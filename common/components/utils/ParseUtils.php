<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 28.04.17
 * Time: 11:38
 */

namespace common\components\utils;


use backend\models\base\Files;
use common\phpQuery\phpQuery;
use keltstr\simplehtmldom\SimpleHTMLDom as SHD;
use yii\web\UploadedFile;

class ParseUtils extends BaseUtils
{

    /**
     * Get product price in other site
     *
     * @param $link
     * @return array
     */
    public function parseProductPrice($link)
    {
        // Create DOM from URL or file
        $html = SHD::file_get_html($link);
        $product =[];
        // Find all selectors
        foreach ($html->find('span#prcIsum') as $element){
            $real_price = strstr( $element->text(), '$');
             $product['price'] =  $real_price;
        }
        foreach ($html->find('h1#itemTitle') as $element){
            $product['name'] = $element->text();
        }
        return $product;

    }
}