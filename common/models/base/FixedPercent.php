<?php

namespace common\models\base;

use Yii;

/**
 * This is the base model class for table "fixed_percent".
 *
 * @property integer $id
 * @property integer $percent
 */
class FixedPercent extends \common\components\base\BaseActiveRecord
{
    use \mootensai\relation\RelationTrait;

    const UAH = 'R01720';
    const EUR = 'R01239';
    const RUR = 'R01720';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['percent'], 'required'],
            [['percent'], 'integer']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fixed_percent';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'percent' => 'Percent',
        ];
    }
}
