<?php

namespace common\models;

use Yii;
use \common\models\base\FixedPercent as BaseFixedPercent;

/**
 * This is the model class for table "fixed_percent".
 */
class FixedPercent extends BaseFixedPercent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['percent'], 'required'],
            [['percent'], 'integer']
        ]);
    }
	
}
