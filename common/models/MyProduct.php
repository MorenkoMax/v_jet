<?php

namespace common\models;

use Yii;
use \common\models\base\MyProduct as BaseMyProduct;

/**
 * This is the model class for table "my_product".
 */
class MyProduct extends BaseMyProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'price'], 'required'],
            [['name', 'price'], 'string', 'max' => 255]
        ]);
    }
	
}
