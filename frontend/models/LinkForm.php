<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class LinkForm extends Model
{
    public $link;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['link', 'required'],
            ['link', 'string', 'min' => 2, 'max' => 255],
        ];
    }
}
