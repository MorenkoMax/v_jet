<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 21.06.17
 * Time: 11:11
 */
//var_dump($product);
$this->title = "parseProductPage" ;
$this->registerJsFile("@web/js/parse.js",["depends" => 'frontend\assets\AppAsset']);

function getPercentValue($price,$percent){
    $intPrice = substr($price, 1);
    $valuePercent = ($intPrice*($percent/100));
    return $intPrice + $valuePercent;
}
$endSum = getPercentValue($product['price'],$percent);
?>
<input type="hidden" value="<?=$link?>"/>
<p>Цена товара</p>
<?php if($product['price']):?>
    <?= '$'.$endSum;?>
    <p>Название товара</p>
    <?= $product['name']?>
<?php else:?>
    <p>Видимо такого класса в цене нету</p>
<?php endif;?>
<p><br>Процент наценки</p>
<?= $percent . '% <br>'?>
<?php
//    $ValCurs = simplexml_load_file('http://www.cbr.ru/scripts/XML_daily.asp');
//    foreach ($ValCurs as $Valute) {
//        foreach ($Valute->attributes() as $b){
//            if($b == 'R01720'){
//                echo $b."\n";
//                echo $Valute->Value."\n";
//            }
//        }
//    }
?>

<br/><button type="button" class="btn btn-success save">Сохранить</button>
