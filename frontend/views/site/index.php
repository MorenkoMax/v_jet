<?php

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;

$this->title = 'MorenkoMax';
?>

<?php $form = ActiveForm::begin(['id' => 'form-link','action'=>'site/parse']); ?>

<?= $form->field($model, 'link')->textInput(['autofocus' => true]) ?>

<div class="form-group">
    <?= \yii\bootstrap\Html::submitButton('Parse', ['class' => 'btn btn-primary', 'name' => 'link-button']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php foreach ($savedProduct as $v):?>
    <hr>
    <p><?= $v['name']?></p>
    <p><?= $v['price']?></p>
    <p>
        <?php
        $uahPrice = priceInUAH($v['price']);
        echo $uahPrice;

        ?>
    </p>
    <hr>
<?php endforeach;?>
<?php
function priceInUAH($price){
    $intPrice = substr($price, 1);

    $ValCurs = simplexml_load_file('http://www.cbr.ru/scripts/XML_daily.asp');
    foreach ($ValCurs as $Valute) {
        foreach ($Valute->attributes() as $b){
            if($b == \common\models\base\FixedPercent::RUR){
                $rurPrice = $intPrice*$Valute->Value;
                echo '1 USD = '.$Valute->Value . ' RUR<br>';
                echo $rurPrice. ' RUR<br>';
            }
            if($b == \common\models\base\FixedPercent::UAH){
                $uahPrice = ($rurPrice/($Valute->Value/10));
                echo '1 RUR = '.$Valute->Value/10 . ' UAH<br>';
                echo '<hr>';
                echo $uahPrice. ' UAH';
            }
        }
    }
}
?>