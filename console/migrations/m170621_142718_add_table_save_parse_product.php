<?php

use yii\db\Migration;

class m170621_142718_add_table_save_parse_product extends Migration
{
    public function safeUp()
    {
        $this->createTable('my_product',[
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'price' => $this->string()->notNull(),
        ]);
    }

    public function safeDown()
    {
        echo "m170621_142718_add_table_save_parse_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170621_142718_add_table_save_parse_product cannot be reverted.\n";

        return false;
    }
    */
}
