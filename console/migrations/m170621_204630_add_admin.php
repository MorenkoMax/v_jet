<?php

use yii\db\Migration;

class m170621_204630_add_admin extends Migration
{
    public function safeUp()
    {
        $this->execute('INSERT INTO user VALUES ("27", "admin123","Cg6dNjiXDxEoseiQpyiBOOjdqaB027iF","$2y$13$Ly.4oMDQpoIHH4t8hEdqTOx6lifb.lg3q08kPike/A3lzr8PZ3/bq", NULL,"admin123@gmail.com",10,1111,1111);');
    }

    public function safeDown()
    {
        echo "m170621_204630_add_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170621_204630_add_admin cannot be reverted.\n";

        return false;
    }
    */
}
