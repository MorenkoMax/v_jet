<?php

use yii\db\Migration;

class m170621_054304_add_table_fixed_persent extends Migration
{
    public function safeUp()
    {
        $this->createTable('fixed_percent',[
            'id' => $this->primaryKey(),
            'percent' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
       $this->dropTable('fixed_percent');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170621_054304_add_table_fixed_persent cannot be reverted.\n";

        return false;
    }
    */
}
